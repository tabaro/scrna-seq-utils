#' A function to load and perform preprocessing of a given CellRanger output bc matrix.
#' @param filtered_bc_matrix A path to the CellRanger filtered_bc_matrix folder
#' @param project_name The name to assign to the Seurat object
#' @param cells_min Minimum number of cells (see Seurat::CreateSeuratObject)
#' @param features_min Minimum number of features (see Seurat::CreateSeuratObject)
#' @param nFeature_min Minimum number of features
#' @param nFeature_max Maximum number of features
#' @param mt_max Maximum mitochondrial percentage
#' @return A preprocessed Seurat object
#' @export
import_10X <- function(filtered_bc_matrix,
                       project_name,
                       cells_min = 3,
                       features_min = 200,
                       nFeature_min = 200,
                       nFeature_max = 5000,
                       mt_max = 10) {
  obj <- Seurat::CreateSeuratObject(counts = Seurat::Read10X(data.dir = filtered_bc_matrix),
                                    project = project_name,
                                    min.cells = cells_min,
                                    min.features = features_min) %>%
           Seurat::PercentageFeatureSet(pattern = "^mt-", col.name = "percent.mt")
  obj <- subset(obj, subset = nFeature_RNA > nFeature_min & nFeature_RNA < nFeature_max & percent.mt < mt_max)
  return(obj)
}
